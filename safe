#!/bin/bash
IMAGE=quay.io/nerdalize/safe:latest
CONTAINER=my-safe

help(){ # show this help
        echo "$(basename $0) -- run a local Hashicorp Vault container"
        echo ""
        echo "usage: $0 SUBCOMMAND [OPTIONS]"
        echo ""
        echo "SUBCOMMAND: "
        # expose the functions that have a documentation comment
        grep -e "^[a-z_]*(){\s*#" $0 | sed 's/^/\t/' | expand -t8 | sed 's/(){\s*#/\t\t/g' | expand -t 20
        exit 1
}

assert_service(){
    if docker start $CONTAINER >/dev/null 2>&1 ; then
        # the existing container is running; if it is unsealed, we are done
        docker exec $CONTAINER manage-vault is_unsealed && return
    else
        run_container
    fi
    # we must wait until it is unsealed
    docker exec $CONTAINER manage-vault wait_until is_unsealed
    auth
}

# start the container
run_container(){
    echo "running $CONTAINER container" >&2
    docker run --cap-add=IPC_LOCK -di -p 127.0.0.1:8200:8200 --name $CONTAINER $IMAGE 2>&1

}

auth(){ # authorize host user with container vault
    vault auth $(token)
}

token(){ # print the root token to access the vault
    docker exec $CONTAINER manage-vault print_token
}

absorb(){ # absorb yaml document from stdin, and save the secrets in the vault
    docker exec -i $CONTAINER vault-upload -n
}

if [ "$(type -t -- $1)" == "function" ]; then
	assert_service
        $@
else
        help
fi
